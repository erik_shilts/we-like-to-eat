# Vegan sugar cookies

Super easy, simple vegan sugar cookies.

*Makes about 20 cookies*

## Ingredients

* 1/2 cup (1 stick) vegan butter (such as Earth Balance), softened (to speed softening, see note)
* 1/2 cup organic cane sugar + more for topping
* 1/4 cup brown sugar
* 1/4 cup pumpkin puree* (acts as egg substitute | sub with unsweetened applesauce with varied results)
* 1 tsp pure vanilla extract
* 1 3/4 cups unbleached all purpose flour + more for rolling into shapes
* 1/2 Tbsp cornstarch or arrowroot powder (for thickening/binding)
* 1 tsp baking powder
* 1/2 tsp baking soda
* 1/4 tsp salt
* 1-2 tsp non-dairy milk (almond milk, Lactaid, or coconut milk)

## Directions

Preheat the oven to 350 degrees F. 

Add softened butter to a large mixing bowl and cream with a mixer.

Add sugar, brown sugar, vanilla, pumpkin puree, and beat for 1 minute.

Set your sifter over something that will catch fall out and add dry ingredients (flour, cornstarch, salt, baking soda and baking powder). Use a spoon to briefly stir, then sift over butter and sugar mixture.

Mix until until incorporated, being careful not to over mix. Then add almond milk and mix until a soft dough is formed. Switch to a wooden spoon if it gets too thick. If it appears to wet, mix in a bit more flour.

Cover and freeze dough for 15 minutes, or refrigerate for 30-45 minutes (up to overnight).

Scoop out heaping 1 Tbsp amounts of chilled dough and roll into balls. Alternatively, roll out dough on a clean, lightly floured surface, and cut out shapes. (NOTE: For shapes, to ensure they keep their form while baking, freeze them on the baking sheet for 10 minutes before baking.)

Arrange cookies on a clean baking sheet (on parchment papger) 2 inches apart to allow for spreading. If you’ve rolled the dough into balls, dip a glass into cane sugar and gently smash down into a disc to help them cook more evenly.

Bake on the center rack for 10-12 minutes, or very slightly golden brown.

Remove from oven and let rest on pan for a few minutes, then transfer to a cooling rack to cool completely. 

## Resources

Borrowed from [Minimalist Baker](http://minimalistbaker.com/1-bowl-vegan-sugar-cookies/). See more of our favorite recipes on our [Pinterest board](https://www.pinterest.com/emilyposner/recipes-we-like)