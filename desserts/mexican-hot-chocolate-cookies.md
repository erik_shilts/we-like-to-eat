# Mexican Hot Chocolate Cookies

Delicious chocolate-y cookies that have a bit of a kick. Easy to make, and a real crowd pleaser.

*Makes 2 dozen cookies*

![](pics/mexican-hot-chocolate-cookies.jpg)

## Ingredients

* 1 stick (4 ounces) unsalted butter (or earth balance butter)
* 4 ounces good-quality unsweetened chocolate, coarsely chopped
* 1 cup packed light brown sugar
* 1/2 cup granulated sugar
* 2 teaspoons pure vanilla extract
* 2 large eggs
* 1 cup all-purpose flour
* 1/2 cup unsweetened cocoa powder
* 1 tablespoon ground cinnamon
* 1 teaspoon chili powder
* 1/2 teaspoon baking soda
* 1/2 teaspoon kosher salt
* 1/4 teaspoon cayenne pepper
* 1 cup semisweet chocolate chips

## Directions

Preheat the oven to 325 degrees F. 

In a heatproof bowl set over simmering water, melt the butter and chocolate together, whisking until glossy and smooth. Alternately, the butter and chocolate can be melted in the microwave (in a microwave-safe bowl) in 25-second increments, whisking between each interval. Cool the chocolate mixture to room temperature. 

In the bowl of a stand mixer fitted with the paddle attachment (or a hand-held mixer), beat the brown sugar, granulated sugar, vanilla extract and eggs on low speed until well combined. Pour in the cooled chocolate and continue to mix until the ingredients are evenly distributed. 

In a medium bowl sift together the flour, cocoa powder, cinnamon, chili powder, baking soda, salt and cayenne pepper. Add the dry ingredients to the chocolate batter and mix on low speed until just combined and no visible flour remains. Fold in the chocolate chips with a rubber spatula or wooden spoon. 

Working in two batches, scoop 12 balls of dough (preferably using a small ice cream scoop with a spring handle, about 1 1/2 tablespoon size) onto a parchment-lined sheet pan, leaving at least 1 1/2 inches of space between each cookie. Bake the cookies, one pan at a time, for approximately 14 minutes, rotating the pan halfway through cooking time. The cookies should be puffy and still fairly soft when removed from the oven. 

Immediately slide the cookies, still on the parchment paper, onto a wire cooling rack. Cool just a bit before serving, 5 to 10 minutes. Cookies can be stored in airtight container for up to three days, but I prefer them on the day they are baked.

## Resources

Adapted from [Cooking Channel](http://www.cookingchanneltv.com/recipes/spicy-mexican-hot-chocolate-cookies.html). See more of our favorite recipes on our [Pinterest board](https://www.pinterest.com/emilyposner/recipes-we-like)